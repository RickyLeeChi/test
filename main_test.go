package main

import (
	"bytes"
	"encoding/json"
	"helloworld/controllers"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"

	"github.com/stretchr/testify/assert"
)

func TestPingRouteGet(t *testing.T) {
	router := setupRouter()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/api/v1/helloworld/greeting", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "welcome", w.Body.String())
}

func TestPingRoutePost(t *testing.T) {
	data := url.Values{}
	data.Set("name", "ricky")

	router := setupRouter()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/api/v1/helloworld/greeting", strings.NewReader(data.Encode())) // URL-encoded payload
	// req.Header.Add("Authorization", "auth_token=\"XXXXXXX\"")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	// req.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "Hello ricky", w.Body.String())
}

func TestPostNameJSON(t *testing.T) {
	param := make(map[string]string)
	param["user"] = "ricky"
	param["email"] = "ricky@test.com"

	// data := url.Values{}
	// data.Set("user", "ricky")
	// data.Set("email", "ricky@test.com")

	router := setupRouter()

	w := httptest.NewRecorder()

	jsonByte, _ := json.Marshal(param)
	req, _ := http.NewRequest("POST", "/api/v1/helloworld/greetingUser", bytes.NewReader(jsonByte)) // JSON payload
	// req.Header.Add("Authorization", "auth_token=\"XXXXXXX\"")
	// req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	// req.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

	router.ServeHTTP(w, req)

	// Convert the JSON response to a map
	var response map[string]string
	err := json.Unmarshal([]byte(w.Body.String()), &response)
	// Grab the value & whether or not it exists
	value, exists := response["status"]
	// Make some assertions on the correctness of the response.

	assert.Nil(t, err)
	assert.True(t, exists)
	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "ricky you are logged in", value)
}

func TestPostNameJSON_V2(t *testing.T) {
	param := make(map[string]string)
	param["user"] = "ricky"
	param["email"] = "ricky@test.com"

	// data := url.Values{}
	// data.Set("user", "ricky")
	// data.Set("email", "ricky@test.com")

	// router := setupRouter()

	w := httptest.NewRecorder()

	jsonByte, _ := json.Marshal(param)
	gin.SetMode(gin.TestMode)
	ginTestContext, ginTestEngine := gin.CreateTestContext(w)
	ginTestEngine.POST("/api/v1/helloworld/greetingUser", controllers.GreetingUser)
	req, _ := http.NewRequest("POST", "/api/v1/helloworld/greetingUser", bytes.NewReader(jsonByte)) // JSON payload
	// req.Header.Add("Authorization", "auth_token=\"XXXXXXX\"")
	// req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	// req.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))
	ginTestContext.Request = req
	controllers.GreetingUser(ginTestContext)

	// router.ServeHTTP(w, req)

	// Convert the JSON response to a map
	var response map[string]string
	err := json.Unmarshal([]byte(w.Body.String()), &response)
	// Grab the value & whether or not it exists
	value, exists := response["status"]
	// Make some assertions on the correctness of the response.

	assert.Nil(t, err)
	assert.True(t, exists)
	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "ricky you are logged in", value)
}
