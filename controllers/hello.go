package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Login struct {
	User  string `json:"user" binding:"required"`
	Email string `json:"email" binding:"required"`
}

// GET /api/v1/helloworld/greeting
// Hello world with name
func Greeting(c *gin.Context) {
	name := c.Param("name")
	c.String(http.StatusOK, "Hello %s", name)
}

// POST /api/v1/helloworld/greeting
// Hello world with name
func GreetingName(c *gin.Context) {
	name := c.PostForm("name")
	c.String(http.StatusOK, "Hello %s", name)
}

// POST /api/v1/helloworld/greetingUser
// Hello world with name and email with JSON format
func GreetingUser(c *gin.Context) {
	var json Login
	if err := c.ShouldBindJSON(&json); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": json.User + " you are logged in"})
}
