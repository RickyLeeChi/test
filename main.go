package main

import (
	"helloworld/controllers"
	"io"
	"os"

	"github.com/gin-gonic/gin"
)

func setupRouter() *gin.Engine {
	// router := gin.New()

	// router.Use(gin.Logger())

	setuplogger()

	router := gin.New()
	router.Use(gin.Logger())

	r := router.Group("/api/v1/helloworld")
	// Routes
	r.GET("/greeting", func(c *gin.Context) {
		c.String(200, "welcome")
	})
	r.GET("/greeting/:name", controllers.Greeting)
	r.POST("/greeting", controllers.GreetingName)
	r.POST("/greetingUser", controllers.GreetingUser)

	return router
}

func setuplogger() {
	// Disable Console Color, you don't need console color when writing the logs to file.
	gin.DisableConsoleColor()

	// Logging to a file.
	f, _ := os.Create("gin.log")
	// gin.DefaultWriter = io.MultiWriter(f)

	// Use the following code if you need to write the logs to file and console at the same time.
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
}

func main() {
	r := setupRouter()
	// Run the server
	r.Run()
}
